# Description

This is a small game made in one day to first experiment with C++ and SDL2. 
A lot of things are missing but the core mechanic is here. It is obviously a 
very light version of well-known existing game.

![screenshot](resources/images/hexagon.png)

# Known issues

The current CMakeLists.txt fails to link the SDL2_ttf librairies, so the raw 
clang.sh is a quick and dirty workaround.
