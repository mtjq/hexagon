#include "wall.h"

#include <cmath>

#include "const.h"

void TowardsCenter(SDL_FPoint& point, double distance) {
  SDL_FPoint direction(point.x - kScreenWidth / 2.0,
      point.y - kScreenHeight / 2.0);
  double direction_norm = sqrt(pow(direction.x, 2) + pow(direction.y, 2));

  direction.x /= direction_norm;
  direction.y /= direction_norm;

  point.x -= distance * direction.x;
  point.y -= distance * direction.y;
}

double DistanceToCenter(SDL_FPoint& point) {
  return sqrt(
      pow(point.x - kScreenWidth / 2.0, 2)
      + pow(point.y - kScreenHeight / 2.0, 2));
}

Wall::Wall() {
  int hexa_side{std::rand() % 6};
  _angle = hexa_side * M_PI / 3.0;
  SimplifyAngle();
  _vertices = std::vector<SDL_Vertex>();
  for (int i{0}; i < 5; i++) {
    double angle((i + hexa_side + 1) * M_PI / 3.0);
    double next_angle((i + hexa_side + 2) * M_PI / 3.0);
    _vertices.push_back(SDL_Vertex(
          SDL_FPoint(
            cos(angle) * kWallFromCenter,
            sin(angle) * kWallFromCenter),
          kOrangeDark,
          SDL_FPoint()));
    _vertices.push_back(SDL_Vertex(
          SDL_FPoint(
            cos(angle) * (kWallFromCenter + kWallThickness),
            sin(angle) * (kWallFromCenter + kWallThickness)),
          kOrangeLight,
          SDL_FPoint()));
    _vertices.push_back(SDL_Vertex(
          SDL_FPoint(
            cos(next_angle) * kWallFromCenter,
            sin(next_angle) * kWallFromCenter),
          kOrangeDark,
          SDL_FPoint()));
    _vertices.push_back(SDL_Vertex(
          SDL_FPoint(
            cos(angle) * (kWallFromCenter + kWallThickness),
            sin(angle) * (kWallFromCenter + kWallThickness)),
          kOrangeLight,
          SDL_FPoint()));
    _vertices.push_back(SDL_Vertex(
          SDL_FPoint(
            cos(next_angle) * kWallFromCenter,
            sin(next_angle) * kWallFromCenter),
          kOrangeDark,
          SDL_FPoint()));
    _vertices.push_back(SDL_Vertex(
          SDL_FPoint(
            cos(next_angle) * (kWallFromCenter + kWallThickness),
            sin(next_angle) * (kWallFromCenter + kWallThickness)),
          kOrangeLight,
          SDL_FPoint()));
  }
  // Center wall around screen's center
  for (SDL_Vertex& vertex: _vertices) {
    vertex.position.x += kScreenWidth / 2.0;
    vertex.position.y += kScreenHeight / 2.0;
  }

}

Wall::~Wall() {}

double Wall::GetDistanceToCenter() {
  return (DistanceToCenter(_vertices[0].position));
}

double Wall::GetAngle() {
  SimplifyAngle();
  return _angle;
}

bool Wall::Update() {
  for (SDL_Vertex& vertex: _vertices) {
    TowardsCenter(vertex.position, kWallSpeed * kFrameDuration);
  }

  return (DistanceToCenter(_vertices[0].position) < kWallDespawnDistance);
}

void Wall::Render(SDL_Renderer* renderer) {
  SDL_RenderGeometry(renderer, nullptr, _vertices.data(), _vertices.size(),
                     nullptr, 0 );
}

void Wall::SimplifyAngle() {
  while (_angle < 0.0 || _angle >= 2.0 * M_PI) {
    if (_angle < 0.0) {
      _angle += 2.0 * M_PI;
    } else {
      _angle -= 2.0 * M_PI;
    }
  }
}
