#include "game.h"

#include <iostream>
#include <ctime>

#include "SDL2/SDL_ttf.h"

#include "const.h"

Game::Game(const char* title, int x, int y, int w, int h)
    : _title{title},
      _window_x{x},
      _window_y{y},
      _screen_width{w},
      _screen_height{h},
      _window{nullptr},
      _renderer{nullptr},
      _game_state{GameState::PLAY},
      _last_frame_ts{SDL_GetTicks()},
      _current_ts{_last_frame_ts},
      _score{0},
      _level{},
      _player{},
      _score_text{} {
  std::srand(std::time(0));
}

Game::~Game() {
  SDL_DestroyRenderer(_renderer);
  SDL_DestroyWindow(_window);
  TTF_Quit();
  SDL_Quit();
  std::cout << "Score: " << _score << std::endl;
}

void Game::Run() {
  Init(SDL_WINDOW_SHOWN);
  GameLoop();
}

void Game::Init(Uint32 flags) {
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    std::cout << "Could not initialise SDL. SDL_Error: "
      << SDL_GetError() << std::endl;
  }
  if (TTF_Init() < 0) {
    std::cout << "Could not initialise TTF. TTF_Error: "
      << TTF_GetError() << std::endl;
  }

  _window = SDL_CreateWindow(_title, _window_x, _window_y,
                             _screen_width, _screen_height, flags);
  if (_window == nullptr) {
    std::cout << "Could not initialise window. SDL_Error: "
      << SDL_GetError() << std::endl;
  }

  _renderer = SDL_CreateRenderer(
      _window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (_renderer == nullptr) {
    std::cout << "Could not initialise renderer. SDL_Error: "
      << SDL_GetError() << std::endl;
  }

  _score_text.Init(_renderer, std::string("Score: 0"));
}

void Game::GameLoop() {
  while (_game_state != GameState::EXIT) {
    _current_ts = SDL_GetTicks();
    if (_current_ts - _last_frame_ts < kFrameDuration)
      continue;
    HandleEvents();
    _player.Move();
    if (_level.Update()) {
      _score++;
      _score_text.SetText(
          _renderer, std::string("Score: ") + std::to_string(_score));
    }
    Render();
    CheckDefeat();
    _last_frame_ts = _current_ts;
  }
}

void Game::HandleEvents() {
  while (SDL_PollEvent(&_event)) {
    switch (_event.type) {
      case SDL_QUIT:
        _game_state = GameState::EXIT;
        break;
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        switch (_event.key.keysym.sym) {
          case SDLK_LEFT:
            _player._left_pressed = (_event.key.state == SDL_PRESSED);
            break;
          case SDLK_RIGHT:
            _player._right_pressed = (_event.key.state == SDL_PRESSED);
            break;
        }
        break;
    }
  }
}

void Game::Render() {
  SDL_SetRenderDrawColor(_renderer,
      kBackgroundColor.r, kBackgroundColor.g, kBackgroundColor.b, 255);
  SDL_RenderClear(_renderer);
  _player.Render(_renderer);
  _level.Render(_renderer);
  _score_text.Render(_renderer);
  SDL_RenderPresent(_renderer);
}

void Game::CheckDefeat() {
  // The closest wall should be the first one in the vector
  if (_level.GetDistanceToCenter() < kPlayerFromCenter
      && kPlayerFromCenter < _level.GetDistanceToCenter() + kWallThickness) {
    double player_angle = _player.GetAngle();
    double player_angle_bis = player_angle + 2.0 * M_PI;
    double wall_angle = _level.GetClosestWallAngle();
    double wall_angle_ = wall_angle + M_PI / 3.0;

    if (!(wall_angle < player_angle && player_angle < wall_angle_
        || wall_angle < player_angle_bis && player_angle_bis < wall_angle_))
      _game_state = GameState::EXIT;
  }
}
