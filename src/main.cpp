#include "const.h"
#include "game.h"

int main() {
  Game game{"Hexagon", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            kScreenWidth, kScreenHeight};
  game.Run();

  return 0;
}
