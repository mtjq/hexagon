#include "player.h"

#include <numbers>

#include "const.h"

Player::Player()
    : _distance_to_center{kPlayerFromCenter},
      _angle{3.0 * M_PI / 2.0},
      _base_size{kPlayerSize},
      _base_angle_size{_base_size / _distance_to_center},
      _angular_speed{kPlayerAngularSpeed},
      _left_pressed{false},
      _right_pressed{false},
      _base_color{kOrangeDark},
      _tip_color{kOrangeLight} {
  InitializeVertices();
  UpdateVertices();
}

Player::~Player() {}

double Player::GetAngle() {
  SimplifyAngle();
  return _angle;
}

void Player::Move() {
  if (! (_left_pressed ^ _right_pressed))
    return;

  _angle += (_left_pressed ? -1 : 1) * _angular_speed * kFrameDuration;
  SimplifyAngle();
}

void Player::Render(SDL_Renderer* renderer) {
  UpdateVertices();
  SDL_RenderGeometry(renderer, nullptr, _vertices.data(), _vertices.size(),
                     nullptr, 0 );
}

void Player::InitializeVertices() {
  _vertices.push_back(SDL_Vertex(
        SDL_FPoint(), _base_color, SDL_FPoint()));
  _vertices.push_back(SDL_Vertex(
        SDL_FPoint(), _base_color, SDL_FPoint()));
  _vertices.push_back(SDL_Vertex(
        SDL_FPoint(), _tip_color, SDL_FPoint()));
}

void Player::UpdateVertices() {
  _vertices[0].position = SDL_FPoint(
      cos(_angle - _base_angle_size / 2.0) * _distance_to_center
        + kScreenWidth / 2.0,
      sin(_angle - _base_angle_size / 2.0) * _distance_to_center
        + kScreenHeight / 2.0);
  _vertices[1].position = SDL_FPoint(
      cos(_angle + _base_angle_size / 2.0) * _distance_to_center
        + kScreenWidth / 2.0,
      sin(_angle + _base_angle_size / 2.0) * _distance_to_center
        + kScreenHeight / 2.0);
  _vertices[2].position = SDL_FPoint(
      cos(_angle) * (_distance_to_center + _base_size) + kScreenWidth / 2.0,
      sin(_angle) * (_distance_to_center + _base_size) + kScreenHeight / 2.0);
}

void Player::SimplifyAngle() {
  while (_angle < 0.0 || _angle >= 2.0 * M_PI) {
    if (_angle < 0.0) {
      _angle += 2.0 * M_PI;
    } else {
      _angle -= 2.0 * M_PI;
    }
  }
}
