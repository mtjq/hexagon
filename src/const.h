#pragma once

#include <cmath>

#include "SDL2/SDL.h"

// Game
const int kScreenWidth{1024};
const int kScreenHeight{1024};
const Uint32 kFrameDuration{static_cast<Uint32>(1000.0 / 60.0)};

// Player
const double kPlayerFromCenter{128.0};
const double kPlayerAngularSpeed{0.004};
const double kPlayerSize{32.0};

// Walls
const double kWallSpawningTimer{1000.0};
const double kWallFromCenter{
  sqrt(pow(kScreenWidth / 2.0, 2) + pow(kScreenHeight / 2.0, 2)) * 1.15};
const double kWallThickness{24.0};
const double kWallSpeed{0.15};
const double kWallDespawnDistance{kPlayerFromCenter * 0.1};

// Colors
const SDL_Color kBackgroundColor{10, 8, 17};
const SDL_Color kTextColor{222, 222, 222};
const SDL_Color kOrangeDark{165, 85, 24, 255};
const SDL_Color kOrangeLight{211, 121, 38, 255};

// Text
const char kFontPath[]{"resources/fonts/fira_code.ttf"};
const int kFontSize{35};
