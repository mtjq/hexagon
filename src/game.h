#pragma once

#include <SDL2/SDL.h>

#include "level.h"
#include "player.h"
#include "text.h"

enum class GameState {
  PLAY,
  EXIT,
};

class Game {
 public:
  Game(const char* title, int x, int y, int w, int h);
  ~Game();

  void Run();

 private:
  void Init(Uint32 flags);
  void GameLoop();
  void HandleEvents();
  void Render();
  void CheckDefeat();

  const char* _title;
  int _window_x;
  int _window_y;
  int _screen_width;
  int _screen_height;
  SDL_Window* _window;
  SDL_Renderer* _renderer;
  SDL_Event _event;
  GameState _game_state;
  Uint32 _last_frame_ts;
  Uint32 _current_ts;

  int _score;
  Level _level;
  Player _player;
  Text _score_text;
};
