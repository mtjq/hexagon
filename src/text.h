#pragma once

#include <string>

#include "SDL2/SDL_ttf.h"

class Text {
 public:
  Text();
  ~Text();

  void Init(SDL_Renderer* renderer, std::string text);
  void SetText(SDL_Renderer* renderer, std::string text);
  void Render(SDL_Renderer* renderer);

 private:
  std::string _text;
  SDL_Surface* _surface;
  SDL_Texture* _texture;
  TTF_Font* _font;
  SDL_Color _color;
  SDL_Rect _rect;
};
