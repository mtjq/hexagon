#pragma once

#include "wall.h"

class Level {
 public:
  Level();
  ~Level();
  double GetDistanceToCenter();
  double GetClosestWallAngle();
  bool Update();
  void Render(SDL_Renderer* renderer);

 private:
  double _wall_spawn_timer;
  std::vector<Wall> _walls;
};
