#pragma once

#include <SDL2/SDL.h>
#include <vector>

void TowardsCenter(SDL_FPoint& point, double distance);

class Wall {
 public:
  Wall();
  ~Wall();
  double GetDistanceToCenter();
  double GetAngle();
  bool Update();
  void Render(SDL_Renderer* renderer);

 private:
  void SimplifyAngle();

  double _angle;
  std::vector<SDL_Vertex> _vertices;
};
