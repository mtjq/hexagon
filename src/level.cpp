#include "level.h"

#include <SDL2/SDL.h>

#include "const.h"

Level::Level() : _wall_spawn_timer{0.0} {
  _walls = std::vector<Wall>();
  _walls.push_back(Wall());
}

Level::~Level() {}

double Level::GetDistanceToCenter() {
  return _walls[0].GetDistanceToCenter();
}

double Level::GetClosestWallAngle() {
  return _walls[0].GetAngle();
}

bool Level::Update() {
  bool score_up = false;

  _wall_spawn_timer += kFrameDuration;
  if (_wall_spawn_timer >= kWallSpawningTimer) {
    _wall_spawn_timer = 0.0;

    _walls.push_back(Wall());
  }

  for (std::vector<Wall>::iterator wall = _walls.begin();
       wall != _walls.end();) {
    wall->Update();
    if (wall->Update()) {
      _walls.erase(wall);
      score_up = true;
    } else {
      wall++;
    }
  }

  return score_up;
}

void Level::Render(SDL_Renderer* renderer) {
  for (std::vector<Wall>::iterator wall = _walls.begin();
       wall != _walls.end(); wall++) {
    wall->Render(renderer);
  }
}
