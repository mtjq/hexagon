#pragma once

#include <vector>
#include "SDL2/SDL.h"

class Player {
 public:
  Player();
  ~Player();

  double GetAngle();
  void Move();
  void Render(SDL_Renderer* renderer);

  bool _left_pressed;
  bool _right_pressed;

 private:
  void InitializeVertices();
  void UpdateVertices();
  void SimplifyAngle();

  std::vector<SDL_Vertex> _vertices;
  double _distance_to_center;
  double _angle;
  double _base_size;
  double _base_angle_size;
  double _angular_speed;
  SDL_Color _base_color;
  SDL_Color _tip_color;
};
