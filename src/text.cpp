#include "text.h"

#include <iostream>

#include "SDL2/SDL.h"

#include "const.h"

Text::Text()
    : _text{},
      _color{kTextColor},
      _rect{50, 50, 100, 30} {}

void Text::Init(SDL_Renderer* renderer, std::string text) {
  _text = text;

  _font = TTF_OpenFont(kFontPath, kFontSize);
  if (_font == nullptr) {
    std::cout << "Could not load font. TTF error: " << TTF_GetError() << "\n";
  }

  _surface = TTF_RenderText_Solid(_font, _text.c_str(), _color);
  if (_surface == nullptr) {
    std::cout << "Could not create text surface. TTF error: "
      << TTF_GetError() << "\n";
  }

  _texture = SDL_CreateTextureFromSurface(renderer, _surface);
  if (_texture == nullptr) {
    std::cout << "Could not create text texture. TTF error: "
      << TTF_GetError() << "\n";
  }
}

void Text::SetText(SDL_Renderer* renderer, std::string text) {
  _text = text;

  _surface = TTF_RenderText_Solid(_font, _text.c_str(), _color);
  if (_surface == nullptr) {
    std::cout << "Could not create text surface. TTF error: "
      << TTF_GetError() << "\n";
  }

  _texture = SDL_CreateTextureFromSurface(renderer, _surface);
  if (_texture == nullptr) {
    std::cout << "Could not create text texture. TTF error: "
      << TTF_GetError() << "\n";
  }
}

Text::~Text() {
  SDL_FreeSurface(_surface);
  SDL_DestroyTexture(_texture);
}

void Text::Render(SDL_Renderer* renderer) {
  SDL_RenderCopy(renderer, _texture, NULL, &_rect);
}
